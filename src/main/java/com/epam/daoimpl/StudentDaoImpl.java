package com.epam.daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.epam.dao.StudentDao;
import com.epam.dto.Student;

@Component
public class StudentDaoImpl implements StudentDao
{
	List<Student> students;
	
	public StudentDaoImpl() {
		students = new ArrayList<Student>();
	}

	public Boolean createStudentProfile(Student student) {
		// TODO Auto-generated method stub
		return students.add(student);
	}

	public Student getStudentById(int sid) {
		// TODO Auto-generated method stub
		return students.stream()
				.filter(s -> s.getRollNo() == sid)
				.findAny()
				.orElse(null);
	}

	@Override
	public List<Student> getAllStudents() {
		// TODO Auto-generated method stub
		return students;
	}

	@Override
	public void deleteStudent(int sid) {
		// TODO Auto-generated method stub
		students.removeIf(s -> s.getRollNo() == sid);
		System.out.println("Student with " + sid + " is removed succesfully");
	}

	public void updateStudent(final Student stu) {
		// TODO Auto-generated method stub
		Student existingStudent = students.stream()
				.filter(s -> s.getRollNo() == stu.getRollNo())
				.toList()
				.get(0);
		existingStudent.setName(stu.getName());
		existingStudent.setCourse(stu.getCourse());
		existingStudent.setYoj(stu.getYoj());
		System.out.println("Student with " + stu.getRollNo() + " is updated succesfully");

	}	
}