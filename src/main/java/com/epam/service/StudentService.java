package com.epam.service;

import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.daoimpl.StudentDaoImpl;
import com.epam.dto.Course;
import com.epam.dto.Student;

@Component
public class StudentService {
	
	Scanner scanner = new Scanner(System.in);
	//StudentDaoImpl createstudent = new StudentDaoImpl();
	@Autowired
	StudentDaoImpl createstudent;
	
	public void menu()
	{
	
		while(true) {
			System.out.println("********** STUDENT MANAGEMENT SYSTEM Project *************** ");
			System.out.print("1. Create Student Profile \n2. Display Student Info By Id \n");
			System.out.print("3. Display All Students Info \n4. Delete Student Info By Id \n");
			System.out.print("5. Update Student Info By Id\n6.To Exit");
		
			System.out.println("\nEnter your Choice: ");
			int n = scanner.nextInt();
			int rno;
		
			switch(n) {
				case 1: //create a student profile
					Student student = createStudent(scanner);
					if(createstudent.createStudentProfile(student)){
						System.out.println("Student created successfully"); 
					} 
					else {
						System.out.println("Student not created successfully"); 
					}
					break;					
				case 2:	//get a student from list by id
					try {
					System.out.println("Enter Student Roll Number to Get Details: ");
					rno = scanner.nextInt();
					Student student_data = createstudent.getStudentById(rno);
					displayStudent(student_data);
					}
					catch(NullPointerException e) {
						System.out.println("Roll Number doesn't Exit\n Please Enter correct Roll Number\n");
					}
					break;
				case 3: //Read students Information					
					List<Student> list = createstudent.getAllStudents();
					if(list.isEmpty()) {
						System.out.println("....OOOOPs List is Empty\n");
						break;
					}
					//System.out.println(sdi.getAllStudents()); //single line
					//list.forEach(s -> System.out.println(s)); //Lambda expression each list in a line
					System.out.println("Reading all students information: ");
					list.forEach(System.out::println); //method reference each list in a line
					break;
				case 4: //delete a student from list
					try {
					System.out.println("Enter Student Roll Number to Delete: ");
					rno = scanner.nextInt();
					if(createstudent.getStudentById(rno)==null) {
						throw new NullPointerException();
					}
					createstudent.deleteStudent(rno);
					List<Student> list1 = createstudent.getAllStudents();
					System.out.println("Students information after deletion: ");
					list1.forEach(System.out::println);
					}
					catch (NullPointerException e) {
						System.out.println("Roll Number doesn't Exit ----> Please Enter correct Roll Number\n");
					}
					break;
				case 5: //update a student from list
					System.out.println("Update a student info from list by id: ");
					System.out.println("Enter Student Roll Number to Update: ");
					rno = scanner.nextInt();
					Student stu =UpdateStudent(rno);
					createstudent.updateStudent(stu);
					break;
				case 6:System.exit(0);
					
			   default: System.out.println("Invalid choice - Please Enter correct choice\n");
			}
		}
	}

	private Student createStudent(Scanner scanner) {
		Student s = new Student();
		Course c = new Course();
		System.out.println("Enter Student Roll Number: ");
		int rollno = scanner.nextInt();
		s.setRollNo(rollno);
		getStudent(s,c);
		return s;
			}
	
	private Student UpdateStudent(int rno) {
		Student s = new Student();
		Course c = new Course();
		s.setRollNo(rno);
		getStudent(s,c);
		return s;
		}
	
	private void getStudent(Student s,Course c) {
		scanner.nextLine();
		System.out.println("Enter Student Name: ");
		String name = scanner.nextLine();
		s.setName(name);
		System.out.println("Enter Course Name: ");
		String coursename = scanner.nextLine();
		c.setCoursename(coursename);
		System.out.println("Enter Course Id: ");
		int courseid = scanner.nextInt();
		c.setCourseid(courseid);
		s.setCourse(c);
		System.out.println("Enter Student YOJ: ");
		int yoj = scanner.nextInt();
		s.setYoj(yoj);
	}
	private void displayStudent(Student student)
	{
		System.out.println(" *****************************************");
		System.out.println("Student Roll Number: "+student.getRollNo());
		System.out.println("Student Name: "+student.getName());
		System.out.println("Student Date of Joining: "+student.getYoj());
		System.out.println("Student Course Number: "+student.getCourse().getCourseid());
		System.out.println("Student Course Name: "+student.getCourse().getCoursename());
		System.out.println(" ******************************************");
	}
}