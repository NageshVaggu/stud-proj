package com.epam.dto;

import org.springframework.stereotype.Component;

@Component
public class Course {
	String coursename;
	int courseid;
	
//	public Course(String coursename, int courseid) {
//		super();
//		this.coursename = coursename;
//		this.courseid = courseid;
//	}

	public String getCoursename() {
		return coursename;
	}

	public void setCoursename(String coursename) {
		this.coursename = coursename;
	}

	public int getCourseid() {
		return courseid;
	}

	public void setCourseid(int courseid) {
		this.courseid = courseid;
	}

	@Override
	public String toString() {
		return "Course [Coursename: " + getCoursename() + ", Courseid: " + getCourseid() + "]";
	}
}

