package com.epam.stud_proj;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.epam.service.StudentService;

@Configuration
@ComponentScan(basePackages="com.epam")
public class App 
{
    public static void main( String[] args )
    {
        //StudentService studentservice = new StudentService();
        //studentservice.menu();
    	ApplicationContext context = new AnnotationConfigApplicationContext(App.class);
    	StudentService studentservice = context.getBean(StudentService.class);
    	studentservice.menu();
    }
}
