package com.epam.dao;

import java.util.List;

import com.epam.dto.Student;


public interface StudentDao {
	
	public Boolean createStudentProfile(Student s);
	
	public Student getStudentById(int sid);
	
	public List<Student> getAllStudents();
	
	public void deleteStudent(int sid);
	
	public void updateStudent(Student stu);

}
